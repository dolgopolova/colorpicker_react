This project was built with React v.16.12.0 and contain custom color picker component.
Color picker allow to select predefined colors in dropdown at the right corner.
Also, user can create own color in rgb palette pop up and get color in hex format.

To view how it works follow this [link](https://bitbucket.org/dolgopolova/colorpicker/src/master/colorpicker/example/colorpicker-functionality.gif).


### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
