import React from 'react';
import './App.scss';
import ColorPicker from './color-picker/ColorPicker';
import DropdownColorModel from 'shared/models/DropdownColor.model';

interface State {
  selectedColor: string,
  colors: DropdownColorModel[]
};

class App extends React.Component<{}, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      selectedColor: "#000000",
      colors: new Array<DropdownColorModel>()
    }
  }

  componentDidMount() {
    let colors = new Array<DropdownColorModel>();
    colors.push(new DropdownColorModel("RED", "#d40000"));
    colors.push(new DropdownColorModel("YELLOW", "#edc40c"));
    colors.push(new DropdownColorModel("GREEN", "#1dc200"));
    colors.push(new DropdownColorModel("BLUE", "#0081c2"));
    this.state.colors.push(...colors);
  }

  render() {
    return (
      <div className="App">
        <ColorPicker
          value={this.state.selectedColor}
          colors={this.state.colors}
          onChange={this.changeColor.bind(this)}>
        </ColorPicker>
      </div>
    );
  }

  private changeColor(color: string) {
    this.setState({
      selectedColor: color
    });
  }
}

export default App;
