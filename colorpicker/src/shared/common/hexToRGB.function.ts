import { hexToRgbReg } from "./constants";
import RGBColor from "shared/models/rgbColor.model";

export function hexToRgb(hex: string): RGBColor{
    var c: any;
    let rgb = new RGBColor();
    if(hexToRgbReg.test(hex)){
        c= hex.substring(1).split('');
        if(c.length === 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        
        rgb.red = (c>>16)&255;
        rgb.green = (c>>8)&255;
        rgb.blue = c&255;
    }
    return rgb;
}