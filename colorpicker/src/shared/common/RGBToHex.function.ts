import RGBColor from "shared/models/rgbColor.model";

export function RGBToHex(color: RGBColor): string {
    let redHex = Number(color.red).toString(16);
    redHex = redHex.length < 2 ? "0" + redHex : redHex;
    let greenHex = Number(color.green).toString(16);
    greenHex = greenHex.length < 2 ? "0" + greenHex : greenHex;
    let blueHex = Number(color.blue).toString(16);
    blueHex = blueHex.length < 2 ? "0" + blueHex : blueHex;
    return ('#' + redHex + greenHex + blueHex);
}