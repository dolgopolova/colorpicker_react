export const LightColorByte = 255;
export const DarkColorByte = 0;
export const hexToRgbReg = /^#([A-Fa-f0-9]{3}){1,2}$/;
export const maxRGBColors = 256;