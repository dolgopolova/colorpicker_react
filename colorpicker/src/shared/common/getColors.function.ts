import { RGBEnum } from "shared/enums/rgb.enum";

export default function getColors(countOfColors: number, rangeColor: RGBEnum): string {
    let colors = '';
    for (let counter = 0; counter < countOfColors; counter++) {
        if (counter !== 0) {
            colors += ", ";
        }

        let hex = '#';
        let hexCounter = Number(counter).toString(16);
        if (hexCounter.length < 2) {
            hexCounter = "0" + hexCounter;
        }
        switch(rangeColor) {
            case RGBEnum.RED: {
                
                hex += hexCounter + '0000';
                break;
            }
            case RGBEnum.GREEN: {
                hex +='00' + hexCounter + '00';
                break;
            }
            case RGBEnum.BLUE: {
                hex +='0000' + hexCounter;
                break;
            }
        }
        colors += hex;
    }
    return colors;
}