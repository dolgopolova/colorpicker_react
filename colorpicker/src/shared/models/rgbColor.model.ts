export default class RGBColor {
    constructor() {
        this.red = 0;
        this.green = 0;
        this.blue = 0;
    }

    red: number;
    green: number;
    blue: number;
}