class DropdownColorModel {
    constructor(name: string, hex: string) {
        this.name = name;
        this.hex = hex.indexOf('#') !== -1 ? hex : `#${hex}`;
    }

    name!: string;
    hex!: string;
}

export default DropdownColorModel;