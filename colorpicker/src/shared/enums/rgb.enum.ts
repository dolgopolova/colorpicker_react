export enum RGBEnum {
    RED = 'red',
    GREEN = 'green',
    BLUE = 'blue'
}