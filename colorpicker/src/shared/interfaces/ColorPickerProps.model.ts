import DropdownColorModel from "../models/DropdownColor.model";

interface ColorPickerPropsModel {
    value: string;
    onChange: Function;
    colors: DropdownColorModel[];
}

export default ColorPickerPropsModel;