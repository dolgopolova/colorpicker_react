import React, { useState } from 'react';
import './Picker.scss';
import getColors from 'shared/common/getColors.function';
import RGBColor from 'shared/models/rgbColor.model';
import { RGBEnum } from 'shared/enums/rgb.enum';
import { maxRGBColors } from 'shared/common/constants';
import { hexToRgb } from 'shared/common/hexToRGB.function';
import { RGBToHex } from 'shared/common/RGBToHex.function';

interface PickerProps {
    colorChange: Function;
    onCancel: Function;
    onDone: Function;
    initialHexColor: string;
}

export const Picker = (props: PickerProps) => {
    const [redColors] = useState(getColors(maxRGBColors, RGBEnum.RED));
    const [greenColors] = useState(getColors(maxRGBColors, RGBEnum.GREEN));
    const [blueColors] = useState(getColors(maxRGBColors, RGBEnum.BLUE));
    var initialRGBColor = hexToRgb(props.initialHexColor);
    const [color, setColor] = useState(initialRGBColor);

    const handleColorChange = (event: any, colorName: RGBEnum) => {
        let newColor = new RGBColor();
        newColor.red = color.red;
        newColor.green = color.green;
        newColor.blue = color.blue;
        switch (colorName) {
            case RGBEnum.RED: {
                    newColor.red = parseInt(event.target.value);
                    setColor(newColor);
                break;
            }
            case RGBEnum.GREEN: {
                    newColor.green = parseInt(event.target.value);
                    setColor(newColor);
                break;
            }
            case RGBEnum.BLUE: {
                    newColor.blue = parseInt(event.target.value);
                    setColor(newColor);
                break;
            }
        }
        props.colorChange(RGBToHex(newColor));
    }

    return (
        <>
            <div className="arrow-container">
                <div className={'arrow picker'}></div>
            </div>
            <div className="picker-container">
                <div className="bars-container">
                    <input className="slider" type="range" min="0" max={maxRGBColors}
                    style={{background: `linear-gradient(to right, ${redColors})`}}
                    onChange={(event) => handleColorChange(event, RGBEnum.RED)}
                    value={color.red}></input>
                    <input className="slider" type="range" min="0" max={maxRGBColors}
                    style={{background: `linear-gradient(to right, ${greenColors})`}}
                    onChange={(event) => handleColorChange(event, RGBEnum.GREEN)}
                    value={color.green}></input>
                    <input className="slider" type="range" min="0" max={maxRGBColors}
                    style={{background: `linear-gradient(to right, ${blueColors})`}}
                    onChange={(event) => handleColorChange(event, RGBEnum.BLUE)}
                    value={color.blue}></input>
                </div>
                <div className="complete-buttons-container">
                    <button className="cancelButton" onClick={() => props.onCancel(props.initialHexColor)}>Cancel</button>
                    <button className="doneButton" onClick={() => props.onDone(RGBToHex(color))}>Ok</button>
                </div>
            </div>
        </>
    );
}