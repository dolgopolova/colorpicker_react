import React from 'react';
import DropdownColorModel from 'shared/models/DropdownColor.model';
import './Dropdown.scss';

interface DropdownProps {
    colors: DropdownColorModel[],
    handleClickOnItem: Function
}

export default function Dropdown(props: DropdownProps) {
    return (
        <>
            <div className="arrow-container">
                <div className={'arrow dropdown'}></div>
            </div>
            <div className="dropdown-container">
                {
                    props.colors.map((color: DropdownColorModel) => {
                        return (
                            <div className="dropdown-popup" key={color.name} onClick={() => props.handleClickOnItem(color.hex)}>
                                <div className="name">{color.name}</div>
                                <div className="color" style={{ background: color.hex }}></div>
                            </div>
                        )
                    })
                }
            </div>
        </>
    );
}