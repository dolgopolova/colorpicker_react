import React from 'react';
import arrow from 'assets/down-arrow.svg';
import './ColorPicker.scss';
import ColorPickerPropsModel from 'shared/interfaces/ColorPickerProps.model';
import Dropdown from 'popups/dropdown/Dropdown';
import * as Popup from 'popups/picker/Picker';

interface State {
    color: string,
    pickerColor: string,
    showDropdown: boolean,
    showPicker: boolean
};

class ColorPicker extends React.Component<ColorPickerPropsModel, State> {
    private node: HTMLDivElement | null = null;
    constructor(props: ColorPickerPropsModel) {
        super(props);
        this.state = {
            color: this.props.value,
            pickerColor: this.props.value,
            showDropdown: false,
            showPicker: false
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleOutsideClick.bind(this));
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleOutsideClick.bind(this));
    }

    render() {
        return (
            <div className="container" ref={node => { this.node = node; }}>
                <div className="colorHEX">
                    {this.props.value}
                </div>
                <div className="buttons-container">
                    <div className="picker-button" onClick={() => {
                        this.handleButtonClick();
                        this.setState({
                            showPicker: !this.state.showPicker,
                            showDropdown: false
                        });
                    }}>
                        <div className="color" style={{ background: this.state.pickerColor }}></div>
                    </div>
                    <div className="dropdown-button" onClick={() => {
                        this.handleButtonClick();
                        this.setState({
                            showDropdown: !this.state.showDropdown,
                            showPicker: false
                        });
                    }}>
                        <img className={`${this.state.showDropdown ? "open" : ""}`} src={arrow} alt="logo" />
                    </div>
                </div>
                <div className="popups-container">
                    {this.state.showPicker && <Popup.Picker 
                            initialHexColor={this.state.color}
                            colorChange={(hexColor: string) => this.setState({pickerColor: hexColor}) }
                            onCancel={(hexColor: string) => this.handleClickOnItem(hexColor)}
                            onDone={(hexColor: string) => this.handleClickOnItem(hexColor)}></Popup.Picker>}
                    {this.state.showDropdown && Dropdown({
                        colors: this.props.colors, 
                        handleClickOnItem: this.handleClickOnItem.bind(this)})}
                </div>
            </div>
        );
    }

    private handleClickOnItem(hex: string) {
        this.props.onChange(hex);
        this.setState({
            color: hex,
            pickerColor: hex,
            showPicker: false,
            showDropdown: false
        });
    }

    private handleOutsideClick(event: any) {
        if (this.node && event.target && this.node.contains(event.target)) {
            return;
        }

        if(this.state.pickerColor !== this.state.color) {
            this.setState({
                pickerColor: this.state.color
            });
        }

        this.setState({
            showPicker: false,
            showDropdown: false
        });
    }

    private handleButtonClick() {
        if (!this.state.showDropdown && !this.state.showPicker) {
          document.addEventListener('click', this.handleOutsideClick.bind(this), false);
        } else {
          document.removeEventListener('click', this.handleOutsideClick.bind(this), false);
        }
    }
}

export default ColorPicker;